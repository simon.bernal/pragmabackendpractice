package com.pragma.sbz.MonolitoPragmaProject.data;

import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;

public class ImageData {

    public static final Image image = Image.builder()
            .id("1")
            .image("c2Rmc2RzYw==")
            .client(1L)
            .build();
    public static final Image image_2 = Image.builder()
            .id("2")
            .image("c2Rmc2RzYw==")
            .client(2L)
            .build();
}
