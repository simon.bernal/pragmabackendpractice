package com.pragma.sbz.MonolitoPragmaProject.data;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.DocumentTypeDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.DocumentType;

public class DocumentTypeData {

    public static final DocumentType documentType_TarjetaIdentidad = DocumentType.builder()
            .idDocType(1L)
            .description("Trajeta identidad")
            .build();
//    public static final DocumentTypeDTO documentTypeDTO_TarjetaIdentidad = DocumentTypeDTO.builder()
//            .idDocType(1L)
//            .description("Trajeta identidad")
//            .build();

    public static final DocumentType documentType_Cedula = DocumentType.builder()
            .idDocType(2L)
            .description("Cedula")
            .build();
//    public static final DocumentTypeDTO documentTypeDTO_Cedula = DocumentTypeDTO.builder()
//            .idDocType(2L)
//            .description("Cedula")
//            .build();

    public static final DocumentType documentType_Pasaporte = DocumentType.builder()
            .idDocType(3L)
            .description("Pasaporte")
            .build();
//    public static final DocumentTypeDTO documentTypeDTO_Pasaporte = DocumentTypeDTO.builder()
//            .idDocType(3L)
//            .description("Pasaporte")
//            .build();

}
