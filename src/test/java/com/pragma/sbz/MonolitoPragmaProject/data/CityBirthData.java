package com.pragma.sbz.MonolitoPragmaProject.data;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.CityBirthDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.CityBirth;

public class CityBirthData {

    public static final CityBirth cityBirth_Medellin = CityBirth.builder()
            .idCity(1L)
            .city("Medellin")
            .build();
//    public static final CityBirthDTO cityBirthDTO_Medellin = CityBirthDTO.builder()
//            .idCity(1L)
//            .city("Medellin")
//            .build();

    public static final CityBirth cityBirth_Bogota = CityBirth.builder()
            .idCity(2L)
            .city("Bogota")
            .build();
//    public static final CityBirthDTO cityBirthDTO_Bogota = CityBirthDTO.builder()
//            .idCity(2L)
//            .city("Bogota")
//            .build();

    public static final CityBirth cityBirth_Cali = CityBirth.builder()
            .idCity(3L)
            .city("Cali")
            .build();
//    public static final CityBirthDTO cityBirthDTO_Cali = CityBirthDTO.builder()
//            .idCity(3L)
//            .city("Cali")
//            .build();
}
