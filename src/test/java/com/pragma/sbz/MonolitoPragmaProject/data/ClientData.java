package com.pragma.sbz.MonolitoPragmaProject.data;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientRequestDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;

import java.time.LocalDate;

public class ClientData {

    public static final Client client_Request = Client.builder()
            .idClient(1L)
            .name("Simon")
            .lastName("Bernal")
            .birthDate(LocalDate.of(2001,12,14))
            .documentType(DocumentTypeData.documentType_Cedula)
            .cityBirth(CityBirthData.cityBirth_Medellin)
            .document("1000412019")
            .build();
//    public static final ClientRequestDTO clientRequestDTO_Request = ClientRequestDTO.builder()
//            .idClient(1L)
//            .name("Simon")
//            .lastName("Bernal")
//            .birthDate(LocalDate.of(2001,12,14))
//            .documentType(DocumentTypeData.documentTypeDTO_Cedula)
//            .cityBirth(CityBirthData.cityBirthDTO_Medellin)
//            .document("1000412019")
//            .build();

    public static final Client client_1 = Client.builder()
            .idClient(2L)
            .name("Manuela")
            .lastName("Bernal")
            .birthDate(LocalDate.of(2003,7,4))
            .documentType(DocumentTypeData.documentType_TarjetaIdentidad)
            .cityBirth(CityBirthData.cityBirth_Medellin)
            .document("1000412019")
            .build();
//    public static final ClientRequestDTO clientRequestDTO_1 = ClientRequestDTO.builder()
//            .idClient(1L)
//            .name("Simon")
//            .lastName("Bernal")
//            .birthDate(LocalDate.of(2001,12,14))
//            .documentType(DocumentTypeData.documentTypeDTO_Cedula)
//            .cityBirth(CityBirthData.cityBirthDTO_Medellin)
//            .document("1000412019")
//            .build();

    public static final Client client_2 = Client.builder()
            .idClient(3L)
            .name("Clara")
            .lastName("Zuluaga")
            .birthDate(LocalDate.of(1966,12,9))
            .documentType(DocumentTypeData.documentType_Cedula)
            .cityBirth(CityBirthData.cityBirth_Medellin)
            .document("43502218")
            .build();

    public static final Client client_3 = Client.builder()
            .idClient(4L)
            .name("David")
            .lastName("Bernal")
            .birthDate(LocalDate.of(1955,12,2))
            .documentType(DocumentTypeData.documentType_Cedula)
            .cityBirth(CityBirthData.cityBirth_Medellin)
            .document("70101914")
            .build();

    public static final Client clientYounger = Client.builder()
            .idClient(5L)
            .name("Mateo")
            .lastName("Zuluaga")
            .birthDate(LocalDate.of(2011,9,13))
            .documentType(DocumentTypeData.documentType_TarjetaIdentidad)
            .cityBirth(CityBirthData.cityBirth_Medellin)
            .document("1234567890")
            .build();
}
