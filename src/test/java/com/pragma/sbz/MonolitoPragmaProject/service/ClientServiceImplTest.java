package com.pragma.sbz.MonolitoPragmaProject.service;

import com.pragma.sbz.MonolitoPragmaProject.data.ClientData;
import com.pragma.sbz.MonolitoPragmaProject.data.DocumentTypeData;
import com.pragma.sbz.MonolitoPragmaProject.data.ImageData;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientAlreadyExistsException;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientYearsException;
import com.pragma.sbz.MonolitoPragmaProject.exception.documenttype.DocumentTypeDontExist;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ClientDTOMapper;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ClientRequestMapper;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientRequestDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.DocumentType;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;
import com.pragma.sbz.MonolitoPragmaProject.repository.ClientRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.DocumentTypeRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.ImageRepository;
import com.pragma.sbz.MonolitoPragmaProject.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@SpringBootTest

class ClientServiceImplTest {
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ImageRepository imageRepository;
    @Mock
    private DocumentTypeRepository documentTypeRepository;

    @InjectMocks
    private ClientServiceImpl clientService;
    @InjectMocks
    private ImageServiceImpl imageService;

    DocumentType documentType;
    Client client, client_1, client_2, client_3;
    List<Client> clientList;
    ClientDTO clientDTO, clientDTO_1;
    ClientRequestDTO  clientRequestDTO;
    MockMultipartFile fileMock;
    Image image, image_2;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        client = ClientData.client_Request;
        client_1 = ClientData.client_1;
        client_2 = ClientData.client_2;
        client_3 = ClientData.client_3;
        clientList = new ArrayList<>();
        clientDTO = ClientDTOMapper.INSTANCE.mapToDto(client);
        clientDTO_1 = ClientDTOMapper.INSTANCE.mapToDto(client_1);
        fileMock = new MockMultipartFile("data", "filename.txt", "text/plain", "sdfsdsc".getBytes());
        clientRequestDTO = ClientRequestMapper.INSTANCE.clientToRequestDTO(client);
        image = ImageData.image;
        image_2 = ImageData.image_2;
        documentType = DocumentTypeData.documentType_Cedula;
    }

    @Test
    void insertClient() throws IOException {
        when(clientRepository.findByDocumentTypeAndDocument(any(DocumentType.class), anyString())).thenReturn(Optional.empty());
//        when(clientRepository.findByDocument(anyString())).thenReturn(Optional.empty());
        when(clientRepository.save(any(Client.class))).thenReturn(client);
//        assertEquals("Simon", clientService.insertClient(ClientRequestMapper.INSTANCE.clientToRequestDTO(client), fileMock).getName());
        assertEquals( clientDTO, clientService.insertClient(clientRequestDTO, fileMock));

    }

    @Test
    void insertClientWhenClienteAlreadyExist() throws IOException {
        when(clientRepository.findByDocumentTypeAndDocument(any(DocumentType.class), anyString())).thenReturn(Optional.of(client));
//        when(clientRepository.findByDocument(anyString())).thenReturn(Optional.of(client));
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        assertThrows(ClientAlreadyExistsException.class, () -> clientService.insertClient(clientRequestDTO, fileMock));
        //assertEquals("Simon",clientService.insertClient(ClientRequestMapper.INSTANCE.clientToRequestDTO(client), fileMock ));
    }
    @Test
    void insertClientWhenYoungerClient() throws IOException {
        client.setBirthDate(LocalDate.of(2011,9,13));
        clientRequestDTO.setBirthDate(LocalDate.of(2011,9,13));
        when(clientRepository.findByDocumentTypeAndDocument(any(DocumentType.class), anyString())).thenReturn(Optional.empty());
//        when(clientRepository.findByDocument(anyString())).thenReturn(Optional.empty());
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        assertThrows(ClientYearsException.class, () -> clientService.insertClient(clientRequestDTO, fileMock));
        //assertEquals("Simon",clientService.insertClient(ClientRequestMapper.INSTANCE.clientToRequestDTO(client), fileMock ));
    }

    @Test
    void getAllClient() throws IOException {
        for (Long i = 1L; i < 5; i++) {
            client.setIdClient((i));
            clientList.add(client);
        }
//        List<Client> c = new ArrayList<Client>(Arrays.asList(client));
//        List<ClientDTO> clientDTOS= ClientDTOMapper.INSTANCE.mapToDTO((List<Client>) c);
        List<ClientDTO> clientDTOS= ClientDTOMapper.INSTANCE.mapToDTO(clientList);
        when(clientRepository.findAll()).thenReturn(clientList);
        // when(ClientDTOMapper.INSTANCE.mapToDTO((List<Client>) clientRepository.findAll())).thenReturn(clientDTOS);
//        List<ClientDTO> cdt = clientService.getAllClient();
        assertArrayEquals(clientDTOS.toArray(), clientService.getAllClient().toArray());
    }


    @Test
    void updateClient() {
        clientRequestDTO.setBirthDate(LocalDate.of(2001,12,14));
        client.setBirthDate(LocalDate.of(2001,12,14));
        clientDTO.setAge(Utils.getAgeByBirthDate(LocalDate.of(2001,12,14)));
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        assertEquals( clientDTO, clientService.updateClient(clientRequestDTO));
    }
    @Test
    void updateClientNotFoundClient() {
        when(clientRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        assertThrows(ClientNotFoundException.class, () -> clientService.updateClient(clientRequestDTO));
    }
    @Test
    void updateClientWhenYoungerClient() {
        client.setBirthDate(LocalDate.of(2011,9,13));
        clientRequestDTO.setBirthDate(LocalDate.of(2011,9,13));
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        assertThrows(ClientYearsException.class, () -> clientService.updateClient(clientRequestDTO));
    }

    @Test
    void deleteClient() throws IOException {
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findByClientNumDoc(anyString())).thenReturn(Optional.ofNullable(image));
        clientService.deleteClient(anyLong());
        verify(clientRepository).deleteById(anyLong());
        verify(imageRepository).deleteById(anyString());
    }
    @Test
    void deleteClientNoImage() throws IOException {
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findByClientNumDoc(anyString())).thenReturn(Optional.empty());
        clientService.deleteClient(anyLong());
        verify(clientRepository).deleteById(anyLong());
    }

    @Test
    void findClientById() throws IOException {
        clientDTO.setImage("c2Rmc2RzYw==");
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(client.getIdClient())).thenReturn(Optional.ofNullable(image));
        assertEquals(clientDTO,clientService.findClientById(anyLong()));
    }
    @Test
    void findClientByIdNotFound() throws IOException {
        when(clientRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ClientNotFoundException.class, () -> clientService.findClientById(anyLong()));
    }
    @Test
    void findClientByIdNotImage() throws IOException {
        clientDTO.setImage("No photo assigned Yet");
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(client.getIdClient())).thenReturn(Optional.empty());
        assertEquals(clientDTO,clientService.findClientById(anyLong()));
    }

    @Test
    void findClientByNumDoc() throws IOException {
        clientDTO.setImage("c2Rmc2RzYw==");
        clientDTO_1.setImage("No photo assigned Yet");
        List<Client> listClients = Arrays.asList(client, client_1);
        when(clientRepository.findAllByDocument(anyString())).thenReturn(Optional.ofNullable(listClients));
        when(imageRepository.findImageByClient(client.getIdClient())).thenReturn(Optional.ofNullable(image));
        when(imageRepository.findImageByClient(client_1.getIdClient())).thenReturn(Optional.empty());
        List<ClientDTO> founds = clientService.findClientByNumDoc(anyString());
        assertEquals(2,founds.size());
        clientDTO.setImage(image.getImage());
        assertEquals(clientDTO, founds.get(0));
        assertEquals(clientDTO_1, founds.get(1));
//        assertEquals(clientDTO,clientService.findClientByNumDoc(anyString()));
    }
    @Test
    void findClientByNumDocClientNotFoundException() throws IOException{
        when(clientRepository.findAllByDocument(anyString())).thenReturn(Optional.empty());;
        assertThrows(ClientNotFoundException.class, () -> clientService.findClientByNumDoc(anyString()));
    }

    @Test
    void findClientByDocTypeAndDocNum() {
        clientDTO.setImage(image.getImage());
        when(documentTypeRepository.findByDescription(anyString())).thenReturn(Optional.ofNullable(documentType));
        when(clientRepository.findByDocumentTypeAndDocument(any(DocumentType.class), anyString())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(client.getIdClient())).thenReturn(Optional.ofNullable(image));
        assertEquals(clientDTO, clientService.findClientByDocTypeAndDocNum( documentType.getDescription(), anyString()));
    }
    @Test
    void findClientByDocTypeAndDocNumDocumentTypeDontExist() {
        when(documentTypeRepository.findByDescription(anyString())).thenReturn(Optional.empty());
        assertThrows(DocumentTypeDontExist.class, () -> clientService.findClientByDocTypeAndDocNum( documentType.getDescription(), anyString()));
    }
    @Test
    void findClientByDocTypeAndDocNumClientNotFoundException() {
        when(documentTypeRepository.findByDescription(anyString())).thenReturn(Optional.ofNullable(documentType));
        when(clientRepository.findByDocumentTypeAndDocument(any(DocumentType.class), anyString())).thenReturn(Optional.empty());
        assertThrows(ClientNotFoundException.class, () -> clientService.findClientByDocTypeAndDocNum( documentType.getDescription(), anyString()));
    }
    @Test
    void findClientByDocTypeAndDocNumNotImage() throws IOException {
        clientDTO.setImage("No photo assigned Yet");
        when(documentTypeRepository.findByDescription(anyString())).thenReturn(Optional.ofNullable(documentType));
        when(clientRepository.findByDocumentTypeAndDocument(any(DocumentType.class), anyString())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(client.getIdClient())).thenReturn(Optional.empty());
        assertEquals(clientDTO, clientService.findClientByDocTypeAndDocNum( documentType.getDescription(), anyString()));
    }

    @Test
    void listAllClientAgeLessThan() {
        List<Client> listClients = Arrays.asList(client_2,client_3);
        when(clientRepository.findAllByBirthDateLessThan(Utils.getAgeToBirthDate(40))).thenReturn(Optional.of(listClients));
        when(imageRepository.findByClientNumDoc(client_2.getDocument())).thenReturn(Optional.ofNullable(image_2));
        when(imageRepository.findByClientNumDoc(client_3.getDocument())).thenReturn(Optional.empty());
        List<ClientDTO> founds = clientService.listAllClientAgeLessThan(40);
        assertEquals(2,founds.size());
    }

    @Test
    void listAllClientAgeLessThanClientNotFoundException() {
        List<Client> listClients = Arrays.asList(client_2,client_3);
        when(clientRepository.findAllByBirthDateLessThan(Utils.getAgeToBirthDate(40))).thenReturn(Optional.empty());
        List<ClientDTO> founds = clientService.listAllClientAgeLessThan(40);
        assertEquals(0,founds.size());
    }

}