package com.pragma.sbz.MonolitoPragmaProject.service;

import com.pragma.sbz.MonolitoPragmaProject.data.ClientData;
import com.pragma.sbz.MonolitoPragmaProject.data.ImageData;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.exception.image.ImageNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ImageMapper;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ImageDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;
import com.pragma.sbz.MonolitoPragmaProject.repository.ClientRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.DocumentTypeRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.ImageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImageServiceImplTest {

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ImageRepository imageRepository;
    @Mock
    private DocumentTypeRepository documentTypeRepository;

    @InjectMocks
    private ClientServiceImpl clientService;
    @InjectMocks
    private ImageServiceImpl imageService;


    MockMultipartFile fileMock;
    Image image, image_2;
    List<Image> imageList;
    Client client;
    ImageDTO imageDTO;


    @BeforeEach
    void setUp() {
        image = ImageData.image;
        image_2 = ImageData.image_2;
        imageList = new ArrayList<Image>();
        client = ClientData.client_Request;
        imageDTO = ImageMapper.INSTANCE.mapToDto(image);
        fileMock = new MockMultipartFile("data", "filename.txt", "text/plain", "sdfsdsc".getBytes());

    }

    @Test
    void getAllImage() throws IOException{
        for (int i = 1; i < 5; i++) {
            String id = i + "";
            String image = "imagen de id " + i;
            Long client = Long.valueOf(i);
            imageList.add(Image.builder().id(id).image(image).client(client).build());
        }
        when(imageRepository.findAll()).thenReturn(imageList);
        List<ImageDTO> imageDTOS= ImageMapper.INSTANCE.mapToDTO(imageList);
        assertEquals(imageDTOS.size(), imageService.getAllImage().size());
        assertThat(imageDTOS).usingRecursiveComparison().isEqualTo(imageService.getAllImage());
    }

    @Test
    void getImage() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(anyLong())).thenReturn(Optional.ofNullable(image));
        assertEquals(imageDTO, imageService.getImage(anyLong()));
    }
    @Test
    void getImageClientNotFoundException() throws IOException {
        when(clientRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ClientNotFoundException.class,() -> imageService.getImage(anyLong()));
    }
    @Test
    void getImageImageNotFoundException() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(anyLong())).thenReturn(Optional.empty());
        assertThrows(ImageNotFoundException.class,() -> imageService.getImage(anyLong()));
    }

    @Test
    void insertImageFile() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(anyLong())).thenReturn(Optional.ofNullable(image));
        when(imageRepository.save(any(Image.class))).thenReturn(image);
        assertEquals(imageDTO, imageService.insertImageFile(anyLong(), fileMock));
    }
    @Test
    void insertImageFileClientNotFoundException() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ClientNotFoundException.class, () -> imageService.insertImageFile(anyLong(), fileMock));
    }
    @Test
    void insertImageFileIfNotPresent() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(anyLong())).thenReturn(Optional.empty());
        when(imageRepository.save(any(Image.class))).thenReturn(image);
        assertEquals(imageDTO, imageService.insertImageFile(anyLong(), fileMock));
    }

    @Test
    void insertImage() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(anyLong())).thenReturn(Optional.ofNullable(image));
        when(imageRepository.save(any(Image.class))).thenReturn(image);
        assertEquals(imageDTO, imageService.insertImage(anyLong(),image.getImage()));
    }
    @Test
    void insertImageClientNotFoundException() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ClientNotFoundException.class, () -> imageService.insertImage(anyLong(),image.getImage()));
    }
    @Test
    void insertImageIfNotPresent() throws IOException{
        when(clientRepository.findById(anyLong())).thenReturn(Optional.ofNullable(client));
        when(imageRepository.findImageByClient(anyLong())).thenReturn(Optional.empty());
        when(imageRepository.save(any(Image.class))).thenReturn(image);
        assertEquals(imageDTO, imageService.insertImage(anyLong(),image.getImage()));
    }

    @Test
    void deleteImage() throws IOException{
        imageService.deleteImage(anyLong());
        verify(imageRepository).deleteImageByClient(anyLong());
    }

}