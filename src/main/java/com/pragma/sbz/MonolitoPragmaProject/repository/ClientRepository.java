package com.pragma.sbz.MonolitoPragmaProject.repository;

import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<List<Client>> findAllByDocument(String numDoc);

    Optional<Client> findByDocumentTypeAndDocument(DocumentType documentType, String numDoc);

    Optional<List<Client>> findAllByBirthDateLessThan(LocalDate birthDate);

}
