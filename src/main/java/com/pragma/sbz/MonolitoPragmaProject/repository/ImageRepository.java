package com.pragma.sbz.MonolitoPragmaProject.repository;

import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends MongoRepository<Image, String> {

    @Query(value = "{'client':?0}")
    Optional<Image> findByClientNumDoc(String numDoc);
    Optional<Image> findImageByClient(Long id);
    void deleteImageByClient(Long id);
}
