package com.pragma.sbz.MonolitoPragmaProject.repository;

import com.pragma.sbz.MonolitoPragmaProject.model.entity.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DocumentTypeRepository extends JpaRepository<DocumentType, Long> {

    Optional<DocumentType> findByDescription (String description);
}
