package com.pragma.sbz.MonolitoPragmaProject.controller;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientRequestDTO;
import com.pragma.sbz.MonolitoPragmaProject.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT,RequestMethod.PATCH})
@RequestMapping(value = "api/clients", consumes = MediaType.ALL_VALUE)
@Validated
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Operation(summary = "Traer clientes")
    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<ClientDTO> getClientes(){
        return clientService.getAllClient();
    }

    @Operation(summary = "Insertar cliente o imagen")
    @PostMapping(produces = { "application/json"}, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.CREATED)
    public ClientDTO insertarFotoAndClient(@RequestPart(value = "image",required = false) MultipartFile image,
                                           @Valid @RequestPart("client") ClientRequestDTO client,
                                           BindingResult bindingResult) throws IOException {
        return clientService.insertClient(client, image);
    }


    @Operation(summary = "Actualizar cliente")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ClientDTO updateClientRequestDTO(@RequestBody ClientRequestDTO clientRequestDTO,
                                            BindingResult bindingResult) {
        return clientService.updateClient(clientRequestDTO);
    }


    @Operation(summary = "Eliminar un objeto Persona e ImagenPersona por ID Persona")
    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteClient(@PathVariable(name = "id")Long id) throws IOException {
        clientService.deleteClient(id);
        return ResponseEntity.ok("Persona correctamente eliminada con id: "+id);
    }

    @Operation(summary = "Obtener Client tipo y numero numero de documento")
    @GetMapping("/typeAndDoc")
    @ResponseStatus(HttpStatus.OK)
    public List<ClientDTO> findClientByDocumentTypeAndNumDoc( @RequestParam(required = false) String documentType,
                                                        @RequestParam String document ){
        List<ClientDTO> clientDTOList = new ArrayList<>();
        if(documentType==null || documentType.isEmpty()){
            return clientService.findClientByNumDoc(document);
        }
        clientDTOList.add(clientService.findClientByDocTypeAndDocNum(documentType,document));
        return clientDTOList;
    }

    @Operation(summary = "Obtener informacion de Cliente mayores a ")
    @GetMapping("/age")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ClientDTO> getClientsByAge(@RequestParam(name = "age")int age){
        return clientService.listAllClientAgeLessThan(age);
    }

    @Operation(summary = "Obtener Cliente por ID")
    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ClientDTO getClientById(@PathVariable(name = "id") Long id) {

        return clientService.findClientById(id);
    }

}
