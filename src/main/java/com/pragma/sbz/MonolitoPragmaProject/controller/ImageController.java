package com.pragma.sbz.MonolitoPragmaProject.controller;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ImageDTO;
import com.pragma.sbz.MonolitoPragmaProject.service.ImageService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RequestMapping(value = "api/clients/images", consumes = MediaType.ALL_VALUE)
public class ImageController {

    @Autowired
    private ImageService imageService;

    @Operation(summary = "Obtener imagen por id de Cliente (Param)")
    @GetMapping
    @ResponseBody @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<List<ImageDTO>> getImage(){
        return ResponseEntity.ok(imageService.getAllImage());
    }

    @Operation(summary = "Obtener imagen por id de Cliente (Param)")
    @GetMapping(value = "/{id}")
    @ResponseBody @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ImageDTO> getImageByDocumento(@PathVariable(name = "id")Long id){
        return ResponseEntity.ok(imageService.getImage(id));
    }

//    @PostMapping(value = "/file/{documento}")

    @Operation(summary = "Insertar y Actualizar imagen a cliente por Id cliente en formato Imagen ")
    @PostMapping(value = "/file", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseBody @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<ImageDTO> insertarFotoAndPersonaFile(@RequestPart(value = "imageFile",required = false) MultipartFile image,
                                                               @RequestParam("idCliente") Long id) throws IOException {
        System.out.println(id);
        return ResponseEntity.ok(imageService.insertImageFile(id,image));
    }

    @Operation(summary = "Insertar y Actualizar imagen a cliente por Id cliente en formato Byte64 ")
    @PostMapping(value=("/base64"), produces = { "application/json"})
    @ResponseBody @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<ImageDTO> insertarFotoAndPersonaBase64(@Valid @RequestParam(value = "imageBase64",required = false) String base64,
                                                                 @RequestParam("idCliente") Long id) throws IOException {
        return ResponseEntity.ok(imageService.insertImage(id,base64));
    }

    @Operation(summary = "Eliminar foto de usuario")
    @DeleteMapping(value = "/{idClient}")
    @ResponseBody @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public String deleteImageByNumDoc(@PathVariable(name = "idClient")Long idClient) throws IOException{
        imageService.deleteImage(idClient);
        return "Imagen eliminada correctamente con id del Ciente: "+idClient;
    }

}
