package com.pragma.sbz.MonolitoPragmaProject.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity @Table(name = "document_type")
@Data @Builder
@NoArgsConstructor @AllArgsConstructor

public class DocumentType implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDocType;

    @Column(unique = true, nullable = false, length = 45)
    private String description;
}
