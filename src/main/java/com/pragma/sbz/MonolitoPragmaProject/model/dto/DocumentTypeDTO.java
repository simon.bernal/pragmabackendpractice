package com.pragma.sbz.MonolitoPragmaProject.model.dto;

import lombok.Builder;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data @Builder
public class DocumentTypeDTO {


    private Long idDocType;

    @Pattern(regexp = "^[a-zA-Z]*$",message = "Solo letras")
    private String description;
}