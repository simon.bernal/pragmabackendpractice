package com.pragma.sbz.MonolitoPragmaProject.model.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data @Builder
public class CityBirthDTO {

    private Long idCity;

    @Pattern(regexp = "^[a-zA-Z]*$",message = "Solo letras")
    private String city;
}
