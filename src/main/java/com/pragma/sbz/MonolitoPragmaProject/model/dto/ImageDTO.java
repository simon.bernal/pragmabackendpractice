package com.pragma.sbz.MonolitoPragmaProject.model.dto;

import lombok.Data;
import javax.validation.constraints.NotBlank;


@Data
public class ImageDTO {

    private String id;

    @NotBlank(message = "Campo Vacio")
    private String image;

    private Long client;
}