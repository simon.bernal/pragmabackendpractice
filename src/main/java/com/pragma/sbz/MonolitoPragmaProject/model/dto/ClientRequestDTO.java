package com.pragma.sbz.MonolitoPragmaProject.model.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Data
@Builder
public class ClientRequestDTO {


    private Long idClient;

    @NotBlank(message = "Campo name Vacio")
    @Pattern(regexp = "^[a-zA-Z]*$", message = "Solo letras")
    private String name;


    @NotBlank(message = "Campo lastName Vacio")
    @Pattern(regexp = "^[a-zA-Z]*$", message = "Solo letras")
    private String lastName;

    private CityBirthDTO cityBirth;

    private LocalDate birthDate;

    private DocumentTypeDTO documentType;

    @NotBlank(message = "Campo document Vacio")
    @Pattern(regexp = "^[0-9a-zA-Z]*$", message = "Solo Numeros y letras")
    private String document;
}
