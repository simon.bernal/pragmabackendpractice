package com.pragma.sbz.MonolitoPragmaProject.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;


@Data @Builder
@AllArgsConstructor @NoArgsConstructor
@Document(collection = "image")
public class Image {

    @Id
    private String id;
    @Field
    private String image;
    @Field
    private Long client;


//    @Field
//    private String name;
//
//    @Field
//    private String imageUrl;
//
//    @Field
//    private String imageId;

//    @Field
//    private String client;
//
//    public Image(String name, String imageUrl, String imageId){
//        this.name = name;
//        this.imageUrl = imageUrl;
//        this.imageId = imageId;
//    }
//
//    @Override
//    public String toString() {
//        return String.format("Image[id='%s', description='%s', mimeType='%s', imageBase64='%s' , client='%s']",id,name,imageUrl,imageId,client);
//    }

}