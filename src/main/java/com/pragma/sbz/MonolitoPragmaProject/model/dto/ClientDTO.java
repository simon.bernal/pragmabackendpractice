package com.pragma.sbz.MonolitoPragmaProject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO{

    private Long idClient;
    @NotBlank(message = "Campo Vacio")
    private String name;
    @NotBlank(message = "Campo Vacio")
    private String lastName;
    @NotBlank(message = "Campo Vacio")
    private CityBirthDTO cityBirth;
    @NotNull @Positive(message = "El valor de la edad debe ser mayoir a 0")
    @Min(value = 1)  @Max(value = 120)
    private int age;
    @NotBlank
    private DocumentTypeDTO documentType;
    @NotBlank
    private String document;
    private String image;
}
