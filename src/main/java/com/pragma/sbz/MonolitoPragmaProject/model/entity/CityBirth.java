package com.pragma.sbz.MonolitoPragmaProject.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;


@Entity @Table(name = "city")
@Data @Builder
@AllArgsConstructor @NoArgsConstructor
public class CityBirth {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCity;
    @Column(unique = true, nullable = false, length = 45)
    private String city;
}
