package com.pragma.sbz.MonolitoPragmaProject.utils;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;

public class Utils {
    public static int getAgeByBirthDate(LocalDate date){
        return Period.between(date,LocalDate.now()).getYears();
    }

    public static LocalDate getAgeToBirthDate(int age) {
        return LocalDate.now().minusYears(age);
    }

    public static String toBase64(MultipartFile imagen) {
        try {
//            return Base64.encodeBase64(imagen.getBytes()).toString();
            return new String(Base64.encodeBase64(imagen.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

