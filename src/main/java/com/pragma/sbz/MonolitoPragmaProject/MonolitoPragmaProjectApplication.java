package com.pragma.sbz.MonolitoPragmaProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonolitoPragmaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonolitoPragmaProjectApplication.class, args);
	}

}
