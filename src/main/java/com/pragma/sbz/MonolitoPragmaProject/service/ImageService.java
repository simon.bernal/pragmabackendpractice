package com.pragma.sbz.MonolitoPragmaProject.service;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ImageDTO;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

public interface ImageService {

    List<ImageDTO> getAllImage();
    public ImageDTO getImage(Long id);
    public ImageDTO insertImage(Long id, String base64)throws IOException;
//    public ImageDTO insertImage(Long id, MultipartFile image)throws IOException;
    public void deleteImage(Long id) throws IOException;

    public ImageDTO insertImageFile(Long id, MultipartFile image) throws IOException;

}