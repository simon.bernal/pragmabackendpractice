package com.pragma.sbz.MonolitoPragmaProject.service;

import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientAlreadyExistsException;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientYearsException;
import com.pragma.sbz.MonolitoPragmaProject.exception.documenttype.DocumentTypeDontExist;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ClientDTOMapper;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ClientRequestMapper;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientRequestDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;
import com.pragma.sbz.MonolitoPragmaProject.repository.ClientRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.DocumentTypeRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.ImageRepository;
import com.pragma.sbz.MonolitoPragmaProject.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {


    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private DocumentTypeRepository documentTypeRepository;


    @Override
    public ClientDTO insertClient(ClientRequestDTO client, MultipartFile image) throws IOException {
        String base64 = Utils.toBase64(image);
        Optional<Client> clientVerify = clientRepository.findByDocumentTypeAndDocument(ClientRequestMapper.INSTANCE.clientRequestDTOToClient(client).getDocumentType(), client.getDocument());
        if (clientVerify.isPresent())
            throw new ClientAlreadyExistsException(clientVerify.get().getDocumentType().getDescription(), clientVerify.get().getDocument());
        if (Utils.getAgeByBirthDate(client.getBirthDate()) <= 18)
            throw new ClientYearsException();
        Client save = clientRepository.save(
                            ClientRequestMapper.INSTANCE.clientRequestDTOToClient(client));
        if (!base64.isEmpty() || image != null)
            imageRepository. save(Image.builder().image(base64).client(save.getIdClient()).build());

        return ClientDTOMapper.INSTANCE.mapToDto(save);
    }

    @Override
    public List<ClientDTO> getAllClient() {
        return ClientDTOMapper.INSTANCE.mapToDTO(
                (List<Client>) clientRepository.findAll());
    }

    @Override
    public ClientDTO updateClient(ClientRequestDTO client) {

        clientRepository.findById(client.getIdClient()).orElseThrow(() -> new ClientNotFoundException(client.getIdClient()));
        if (Utils.getAgeByBirthDate(client.getBirthDate()) <= 18)
            throw new ClientYearsException();

        return ClientDTOMapper.INSTANCE.mapToDto(
                clientRepository.save(
                        ClientRequestMapper.INSTANCE.clientRequestDTOToClient(client)));
    }

    @Override
    public void deleteClient(Long id) throws IOException {
        clientRepository.findById(id).ifPresent(client -> {
            Optional<Image> img = imageRepository.findByClientNumDoc(
                    client.getDocument());
            img.ifPresent(image -> imageRepository.deleteById(image.getId()));
            clientRepository.deleteById(id);});
    }

    @Override
    public ClientDTO findClientById(Long id) {
        ClientDTO clientVerify = ClientDTOMapper.INSTANCE.mapToDto(clientRepository.findById(id).orElseThrow(() -> new ClientNotFoundException(id)));
        Optional<Image> image = imageRepository.findImageByClient(clientVerify.getIdClient());

        if (!image.isPresent()) {
            clientVerify.setImage("No photo assigned Yet");
            return clientVerify;
        }
        clientVerify.setImage(image.get().getImage());
        return clientVerify;
    }

    @Override
    public List<ClientDTO> findClientByNumDoc(String numDoc) {
        List<ClientDTO> clientDTOList = ClientDTOMapper.INSTANCE.mapToDTO(
                                    clientRepository.findAllByDocument(numDoc).orElseThrow(
                                            () -> new ClientNotFoundException(numDoc)));
        for (ClientDTO clientDTO : clientDTOList) {
            Optional<Image> image = imageRepository.findImageByClient(clientDTO.getIdClient());
            if (!image.isPresent()) {
                clientDTO.setImage("No photo assigned Yet");
            } else {
                clientDTO.setImage(image.get().getImage());
            }
        }
        return clientDTOList;
    }

    @Override
    public ClientDTO findClientByDocTypeAndDocNum(String documentType, String numDoc) {

        ClientDTO clientDTO = ClientDTOMapper.INSTANCE.mapToDto(
                                    clientRepository.findByDocumentTypeAndDocument(
                                            documentTypeRepository.findByDescription(documentType)
                                                    .orElseThrow(() -> new DocumentTypeDontExist(documentType)), numDoc)
                                            .orElseThrow(() -> new ClientNotFoundException(documentType, numDoc)));

        Optional<Image> image = imageRepository.findImageByClient(clientDTO.getIdClient());
        if (!image.isPresent()) {
            clientDTO.setImage("No photo assigned Yet");
            return clientDTO;
        }
        clientDTO.setImage(image.get().getImage());
        return clientDTO;

    }

    @Override
    @Transactional
    public List<ClientDTO> listAllClientAgeLessThan(int age) {
        List<ClientDTO> lstDTO = ClientDTOMapper.INSTANCE.mapToDTO(clientRepository.findAllByBirthDateLessThan(Utils.getAgeToBirthDate(age)).orElse(new ArrayList<Client>()));
        for (ClientDTO clientDTO : lstDTO) {
            Optional<Image> image = imageRepository.findImageByClient(clientDTO.getIdClient());
            if (!image.isPresent()) {
                clientDTO.setImage("No photo assigned yet");
            } else {
                clientDTO.setImage(image.get().getImage());
            }
        }
        return lstDTO;
    }
}


