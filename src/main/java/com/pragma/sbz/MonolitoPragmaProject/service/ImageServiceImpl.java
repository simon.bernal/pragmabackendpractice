package com.pragma.sbz.MonolitoPragmaProject.service;

import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.exception.image.ImageNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ClientDTOMapper;
import com.pragma.sbz.MonolitoPragmaProject.mapper.ImageMapper;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ImageDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;
import com.pragma.sbz.MonolitoPragmaProject.repository.ClientRepository;
import com.pragma.sbz.MonolitoPragmaProject.repository.ImageRepository;
import com.pragma.sbz.MonolitoPragmaProject.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ImageServiceImpl implements ImageService{

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public List<ImageDTO> getAllImage() {
        return ImageMapper.INSTANCE.mapToDTO(
                (List<Image>) imageRepository.findAll());
    }

    @Override
    public ImageDTO getImage(Long id) {

        Client client = clientRepository.findById(id).orElseThrow(()-> new ClientNotFoundException(id));

        return ImageMapper.INSTANCE.mapToDto(
                imageRepository.findImageByClient(
                        client.getIdClient()).orElseThrow(
                                ()-> new ImageNotFoundException(
                                        client.getDocument())));
    }

    @Override
    public ImageDTO insertImageFile(Long id, MultipartFile image) throws IOException {
        return insertImage(id, Utils.toBase64(image));
    }

    @Override
    public ImageDTO insertImage(Long id, String base64) throws IOException {
        Client client = clientRepository.findById(id).orElseThrow(()->new ClientNotFoundException(id));
        Optional<Image> image = imageRepository.findImageByClient(client.getIdClient());
        if(image.isPresent()) {
            imageRepository.deleteById(image.get().getId());
        }
        return ImageMapper.INSTANCE.mapToDto(
            imageRepository.save(
                    Image.builder().image(base64).client(client.getIdClient()).build()));

    }

    @Override
    public void deleteImage(Long idClient) throws IOException {
        imageRepository.deleteImageByClient(idClient);
    }

}
