package com.pragma.sbz.MonolitoPragmaProject.service;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientRequestDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ClientService {

    ClientDTO insertClient(ClientRequestDTO client, MultipartFile image) throws IOException;

    List<ClientDTO> getAllClient();

    ClientDTO updateClient(ClientRequestDTO client);

    void deleteClient(Long id) throws IOException;

    ClientDTO findClientById(Long id);

    List<ClientDTO> findClientByNumDoc(String numDoc);

    ClientDTO findClientByDocTypeAndDocNum(String documentType, String numDoc);

    List<ClientDTO> listAllClientAgeLessThan(int age);

}