package com.pragma.sbz.MonolitoPragmaProject.mapper;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.CityBirthDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.CityBirth;

public interface CityBirthMapper {

    public CityBirthDTO mapDtoToEntity(CityBirth cityBirth);

    public CityBirth mapEntityToDTO(CityBirthDTO cityBirthDTO);
}
