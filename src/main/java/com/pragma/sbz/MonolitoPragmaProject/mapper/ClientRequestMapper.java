package com.pragma.sbz.MonolitoPragmaProject.mapper;


import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientRequestDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClientRequestMapper {

    ClientRequestMapper INSTANCE = Mappers.getMapper(ClientRequestMapper.class);

    Client clientRequestDTOToClient(ClientRequestDTO clientRequestDTO);

    ClientRequestDTO clientToRequestDTO(Client client);
}