package com.pragma.sbz.MonolitoPragmaProject.mapper;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.DocumentTypeDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.DocumentType;

public interface DocumentTypeMapper {

    public DocumentTypeDTO mapDtoToEntity(DocumentType documentType);

    public DocumentType mapEntityToDTO(DocumentTypeDTO documentTypeDTO);
}