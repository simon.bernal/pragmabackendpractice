package com.pragma.sbz.MonolitoPragmaProject.mapper;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ClientDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Client;
import com.pragma.sbz.MonolitoPragmaProject.utils.Utils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface ClientDTOMapper  {
    ClientDTOMapper INSTANCE = Mappers.getMapper(ClientDTOMapper.class);


    @Mapping(source = "birthDate",target = "age",qualifiedByName = "birthDateToAge")
    public ClientDTO mapToDto(Client client);

    @Named("birthDateToAge")
    public static int birthDateToAge(LocalDate date){
        return Utils.getAgeByBirthDate(date);
    }


    @Mapping(source = "birthDate",target = "age",qualifiedByName = "birthDateToAge")
    public List<ClientDTO> mapToDTO(List<Client> listClient);

    @Mapping(source = "age",target = "birthDate", qualifiedByName = "ageToBirthDate")
    public Client DTOtoEntity(ClientDTO clientDTO);

    @Named("ageToBirthDate")
    public static LocalDate ageToBirthDate(int age){
        return Utils.getAgeToBirthDate(age);
    }
}