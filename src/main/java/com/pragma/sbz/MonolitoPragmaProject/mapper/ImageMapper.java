package com.pragma.sbz.MonolitoPragmaProject.mapper;

import com.pragma.sbz.MonolitoPragmaProject.model.dto.ImageDTO;
import com.pragma.sbz.MonolitoPragmaProject.model.entity.Image;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ImageMapper {

    ImageMapper INSTANCE = Mappers.getMapper(ImageMapper.class);    //Patron de diseño SingleTon, asegurarse que solo existe una instancia.

    ImageDTO mapToDto(Image image);

    List<ImageDTO> mapToDTO(List<Image> listImage);
}