package com.pragma.sbz.MonolitoPragmaProject.exception;

public class ApiRequestException extends RuntimeException{

    public ApiRequestException(String message) {
        super(message);
    }
}