package com.pragma.sbz.MonolitoPragmaProject.exception;

import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientAlreadyExistsException;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientNotFoundException;
import com.pragma.sbz.MonolitoPragmaProject.exception.client.ClientYearsException;
import com.pragma.sbz.MonolitoPragmaProject.exception.documenttype.DocumentTypeDontExist;
import com.pragma.sbz.MonolitoPragmaProject.exception.image.ImageConvertionFailException;
import com.pragma.sbz.MonolitoPragmaProject.exception.image.ImageDeleteException;
import com.pragma.sbz.MonolitoPragmaProject.exception.image.ImageNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final String ERROR_OCCURRED_CONTACT_ADMIN = "An error occurred, please contact administrator.";
    private static final ConcurrentHashMap<String, Integer> STATUS_CODE = new ConcurrentHashMap<>();

    public ErrorHandler() {
        STATUS_CODE.put(ApiRequestException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        STATUS_CODE.put(ClientNotFoundException.class.getSimpleName(),HttpStatus.NOT_FOUND.value());
        STATUS_CODE.put(ClientYearsException.class.getSimpleName(),HttpStatus.BAD_REQUEST.value());
        STATUS_CODE.put(ClientAlreadyExistsException.class.getSimpleName(),HttpStatus.CONFLICT.value());
        STATUS_CODE.put(ImageNotFoundException.class.getSimpleName(),HttpStatus.NOT_FOUND.value());
        STATUS_CODE.put(ImageDeleteException.class.getSimpleName(),HttpStatus.INTERNAL_SERVER_ERROR.value());
        STATUS_CODE.put(ImageConvertionFailException.class.getSimpleName(),HttpStatus.INTERNAL_SERVER_ERROR.value());
        STATUS_CODE.put(DocumentTypeDontExist.class.getSimpleName(),HttpStatus.NOT_FOUND.value());
    }

    //Cuando sale un error sale esta excepcion
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiError> handleConstraintViolationException(ConstraintViolationException exception) {

        String exceptionName = exception.getClass().getSimpleName();
        String message= exception.getMessage();
        ApiError error = new ApiError(exceptionName,message);
        return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleApiRequestException(Exception e) {

        ResponseEntity<ApiError> result;
        String exceptionName = e.getClass().getSimpleName();
        String message = e.getMessage();
        Integer code = STATUS_CODE.get(exceptionName);
        e.printStackTrace();

        if(code !=null){
            ApiError error = new ApiError(exceptionName,message);
            result = new ResponseEntity<>(error,HttpStatus.valueOf(code));
            e.printStackTrace();
        }else{
            ApiError error = new ApiError(exceptionName,ERROR_OCCURRED_CONTACT_ADMIN);
            result = new ResponseEntity<>(error,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return result;
    }
}