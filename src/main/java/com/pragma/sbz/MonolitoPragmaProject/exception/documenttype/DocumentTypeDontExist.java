package com.pragma.sbz.MonolitoPragmaProject.exception.documenttype;

import java.util.function.Supplier;

public class DocumentTypeDontExist extends RuntimeException {

    public DocumentTypeDontExist(String type) {
        super(String.format("The Document Type: %s, does not exists", type));
    }
}
