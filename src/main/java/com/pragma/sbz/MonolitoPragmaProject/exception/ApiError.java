package com.pragma.sbz.MonolitoPragmaProject.exception;

public class ApiError {

    private final String exceptionName;
    private final String message;

    public ApiError(String exceptionName, String message) {
        this.exceptionName = exceptionName;
        this.message = message;
    }

    public String getExceptionName() {
        return exceptionName;
    }

    public String getMessage() {
        return message;
    }
}