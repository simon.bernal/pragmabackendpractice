package com.pragma.sbz.MonolitoPragmaProject.exception.image;

public class ImageNotFoundException extends RuntimeException{
    public ImageNotFoundException(String id) {
        super(String.format("Image associated with client number document: %s not found",id));
    }
}
