package com.pragma.sbz.MonolitoPragmaProject.exception.client;

public class ClientAlreadyExistsException extends RuntimeException{

    public ClientAlreadyExistsException(String DocType, String ClientNumDoc) {
        super(String.format("Client with Num Document: %s number: %s Already Exists", DocType,ClientNumDoc));
    }
}
