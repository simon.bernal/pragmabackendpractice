package com.pragma.sbz.MonolitoPragmaProject.exception.client;

public class ClientYearsException extends RuntimeException{

    public ClientYearsException() {
        super("El cliente a insertar debe ser mayor de 18 años ");
    }
}