package com.pragma.sbz.MonolitoPragmaProject.exception.image;

public class ImageConvertionFailException extends RuntimeException{
    public ImageConvertionFailException() {
        super("The image couldn't convert to base64");
    }
}